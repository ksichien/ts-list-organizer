using EternalGroveApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Games.UnitTests
{
    public class GamesControllerTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public GamesControllerTest()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task ReturnStatusCodeOK()
        {
            // Act
            var response = await _client.GetAsync("/api/v1/Games");
            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
