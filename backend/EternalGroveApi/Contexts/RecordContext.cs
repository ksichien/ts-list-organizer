﻿using EternalGroveApi.Models;
using Microsoft.EntityFrameworkCore;

namespace EternalGroveApi.Contexts
{
    public class RecordContext : DbContext
    {
        public RecordContext(DbContextOptions<RecordContext> options): base(options)
        {
        }

        public DbSet<Record> Records { get; set; }
    }
}
