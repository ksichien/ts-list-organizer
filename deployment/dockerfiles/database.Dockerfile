FROM mysql:8.0
COPY db/games.sql /docker-entrypoint-initdb.d/games.sql
COPY db/records.sql /docker-entrypoint-initdb.d/records.sql
