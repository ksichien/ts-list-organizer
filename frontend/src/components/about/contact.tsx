import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons';

class ContactComponent extends Component {
  render() {
    return (
      <address>
        <h3 className="display-4 py-4">Contact</h3>
        <div className="bg-dark py-4">
          <div className="container">
            <p>For more information, you can visit my <a className="badge badge-light" href="https://gitlab.com/ksichien">GitLab</a> and <a className="badge badge-light" href="https://linkedin.com/in/ksichien">LinkedIn</a> profiles.</p>
            <a href="https://gitlab.com/ksichien" className="px-2 py-4 text-light"><FontAwesomeIcon icon={faGitlab} size="lg" /></a>
            <a href="https://linkedin.com/in/ksichien" className="px-2 py-4 text-light"><FontAwesomeIcon icon={faLinkedin} size="lg" /></a>
          </div>
        </div>
      </address>
    );
  }
}

export default ContactComponent;
