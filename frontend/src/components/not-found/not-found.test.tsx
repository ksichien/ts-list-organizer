import React from 'react';
import { shallow } from 'enzyme';
import NotFoundComponent from './not-found';

describe('<NotFoundComponent />', () => {
  it('renders one h1 element', () => {
    const wrapper = shallow(<NotFoundComponent />);
    expect(wrapper.contains(<h1 className="display-4">404 not found</h1>)).toBeTruthy;
  });
});
