import React from 'react';
import { Navbar } from 'react-bootstrap';
import { shallow } from 'enzyme';
import HeaderComponent from './header';

describe('<HeaderComponent />', () => {
  it('renders one nav brand', () => {
    const wrapper = shallow(<HeaderComponent />);
    expect(wrapper.contains(<Navbar.Brand href="/">Eternal Grove</Navbar.Brand>)).toBeTruthy;
  });
});
