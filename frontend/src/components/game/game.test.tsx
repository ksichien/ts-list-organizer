import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import GameComponent from './game';
import GameListComponent, { Game, parseGames } from './gameList';

describe('<GameComponent />', () => {
  const flushPromises = () => new Promise(resolve => setImmediate(resolve));
  let wrapper: ShallowWrapper;
  beforeEach(async () => {
    wrapper = shallow(<GameComponent />);
    await flushPromises();
    wrapper.update();
  });
  it('renders one gamelist component', async () => {
    expect(wrapper.contains(<GameListComponent />)).toBeTruthy;
  });
  it('displays a game', async () => {
    expect(wrapper.contains(<th key={`th-title-0`} scope="row">Final Fantasy XIV</th>)).toBeTruthy;
  });
  it('adds a placeholder for a blank developer column', async () => {
    expect(wrapper.contains(<th key={`th-developer-1`} scope="row">Unknown</th>)).toBeTruthy;
  });
  it('adds a placeholder for a blank publisher column', async () => {
    expect(wrapper.contains(<th key={`th-publisher-1`} scope="row">Unknown</th>)).toBeTruthy;
  });
});

describe('parseGames', () => {
  it('parses and sorts a list of games', () => {
    const input: any = [
      {
        "title": "Baldurs Gate II: Shadows of Amn",
        "developer": "BioWare",
        "publisher": "Black Isle Studios",
        "genre": "RPG"
      },
      {
        "title": "Sekiro: Shadows Die Twice",
        "developer": "FromSoftware",
        "publisher": "Activision",
        "genre": "Action-Adventure"
      },
      {
        "title": "Dragon Age: Origins",
        "developer": "BioWare",
        "publisher": "Electronic Arts",
        "genre": "RPG"
      },
      {
        "title": "Tenchu: Stealth Assassins",
        "developer": "Acquire",
        "publisher": "Activision",
        "genre": "Action-Adventure"
      }
    ];
    const output: Game[] = [
      {
        "title": "Baldurs Gate II: Shadows of Amn",
        "developer": "BioWare",
        "publisher": "Black Isle Studios",
        "genre": "RPG"
      },
      {
        "title": "Dragon Age: Origins",
        "developer": "BioWare",
        "publisher": "Electronic Arts",
        "genre": "RPG"
      },
      {
        "title": "Sekiro: Shadows Die Twice",
        "developer": "FromSoftware",
        "publisher": "Activision",
        "genre": "Action-Adventure"
      },
      {
        "title": "Tenchu: Stealth Assassins",
        "developer": "Acquire",
        "publisher": "Activision",
        "genre": "Action-Adventure"
      }
    ];
    expect(parseGames(input)).toEqual(output);
  });
});
