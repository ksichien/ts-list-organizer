import React, { Component } from 'react';

interface ErrorBoundaryState {
  error?: any,
  info?: any
}

class ErrorBoundary extends Component<{}, ErrorBoundaryState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      error: undefined,
      info: undefined
    };
  }

  componentDidCatch(error: Error, info: any) {
    this.setState({
      error: error,
      info: info
    });
  }

  render() {
    if (this.state.error) {
      return (
        <section className="bg-dark vh-100">
          <div className="text-light vertical-center">
            <h3 className="display-4">{this.state.error.message}</h3>
          </div>
        </section>
      );
    }
    else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
